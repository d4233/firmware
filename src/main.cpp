#include "main.h"

#include <Arduino.h>
#include <CAN.h>

// get_motor_ids spins each motor in the order of ids (1..4) for 4000ms each.
// used to identify pins since the fucking pcb is not documented.
void get_motor_ids() {
    for (int i = 1; i <= 4; i++) {
        Serial.printf("this is motor %d at speed %d\n", i, 100);
        g_bot.motor(i, 100);
        delay(4000);
        g_bot.motor(i, -100);
        delay(10);
        g_bot.motor(i, 0);
    }
}

void setup() {
    g_bot.set_bot_type(4);  // four wheels
    g_bot.setze_kompass();  // resets the compass to 0

    g_pixy.init();

    while (!Serial)
        ;  // do nothing until there is a serial connection available.
    Serial.printf("starting can bus.\n");
    if (!CAN.begin(500E3)) {
        Serial.printf("can bus failed to start. I will now do nothing forever :)\n");
        while (1)
            ;
    }  // start CAN bus at 500 kbps
    else {
        Serial.println("can bus started successfully.");
    }
}

void debug_output() { Serial.printf("ball direction: %d", g_ball_direction); }

bool get_ball_direction() {
    g_bot.warte(10);
    int ir_data = 0;
    int ball_seen = 0;

    CAN.beginPacket(0x03, 1, true);
    CAN.endPacket();

    // wait a maximum of 10 milliseconds, and then return false if the IR module doesn't return
    // anything.
    for (int i = 0; i < 10; i++) {
        if (CAN.parsePacket()) {
            continue;
        } else if (i == 9) {
            return false;
        }
        delayMicroseconds(1000);
    }

    // parse data
    while (CAN.available()) {
        ir_data = CAN.read();
        g_ball_direction = (ir_data / 16) - 7;
        int zone = ir_data % 16;
        if (zone < 1)
            ball_seen = 0;
        else
            ball_seen = 1;
        if (ball_seen < 1) g_ball_direction = -8;
    }
}

// direction_correction tries to adjust for error in the values received.
// maps (-n:n) to (-n+8:n-8) if that's understandable
int direction_correction(const int& d) {
    if (d >= -3 && d <= 4) return d;
    if (d < -3) return d + 8;
    if (d > 4) return d - 8;
}

// returns a side (forwards, left, right) for a given direction in the format no one understands
int side(const int& d) {
    if (d % 4 == 0) return 0;
    if (d > 0) return 1;
    if (d < 0) return -1;
    return -1337;
}

// fuckery no one understands
//
// ERFAHRUNG
void action() {
    if (!get_ball_direction()) {
        if (g_sees_tor) {
            g_bot.fahre(g_tor_direction, MAX_SPEED, 0);
        } else {
            // fahr to predefined tor position using compass to navigate otherwise
            g_bot.fahre(g_compass, MAX_SPEED, 0);
        }
    }

    while (g_ball_direction != 0) {
        // get behind ball to be able to push it into the goal omg
        int dir = ((g_ball_direction + 1) / 2) + (1 * side(g_ball_direction));
        dir = direction_correction(dir);
        g_bot.fahre(dir, MAX_SPEED * SPEED_TRAP, 0 - g_bot.kompass() / 5);
    }

    // TODO: photoresistor

    g_bot.fahre(g_tor_direction, MAX_SPEED, 0);
}

void loop() {
    Serial.printf("taster %s %s\n", g_bot.taster(1, 2) ? "true" : "false",
                  g_bot.taster(2, 2) ? "true" : "false");

    action();
}