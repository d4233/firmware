#ifndef MAIN_H
#define MAIN_H

#include <PixyI2C.h>

#include "bohlebots.h"

#define MAX_SPEED 50     // maximaler speed (richtung 0)
#define SPEED_TRAP 0.67  // anteil von max_speed (wenn präzise hinter den ball fahren)

extern BohleBots g_bot = BohleBots();

extern PixyI2C g_pixy = PixyI2C();

extern int g_ball_direction = 0;
extern int g_compass;

extern bool g_sees_tor;
extern int g_tor_direction;
extern int g_tor_width;
extern int g_tor_distance;
#endif