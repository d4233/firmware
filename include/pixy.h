// fick deine mutter
#include "main.h"

void pixy_auswerten()  // wird nur aufgerufen, wenn die Pixy überhaupt etwas sieht
{
    int my_signature = 1;  // always use the first (=biggest) signature
    int sieht_farbe = g_pixy.blocks[0].signature;

    if (sieht_farbe == my_signature) {
        g_tor_direction = -(g_pixy.blocks[0].x - 158) / 2;
        g_tor_width = pixy.blocks[0].width;
        g_tor_distance = pow(pixy.blocks[0].y - 80, 6) / 1000000;

        /*Serial.println("torrichtung:"+String(tor_richtung));
        Serial.println("torbreite:"+String(tor_breite));
        Serial.println("tor_entfernung:"+String(tor_entfernung));
        */

        g_compass = -g_tor_direction;
        g_sees_tor = true;
        // topspeed=hightop;
        // seekspeed=seektop;
    } else  // soll hier überhaupt etwas geschehen?
    {
        g_tor_direction = g_compass;

        Wire.beginTransmission(0x27);
        Wire.write(255 - 2);
        Wire.endTransmission();

        g_sees_tor = false;
        // topspeed=lowtop;
        // seekspeed=lowtop;
    }
    // pixyzeit=0;
}

void pixy_read(int &tor_richtung, int &tor_breite, int &tor_entfernung, int compass,
               uint8_t &led_byte) {
    int i;
    // grab blocks!
    g_pixy.getBlocks();

    // If there are detect blocks, print them!
    if (sizeof(g_pixy.blocks) > 0) {
        pixy_auswerten();
    } else {
        tor_richtung = compass;
        led_byte += 2;  // ledbyte um 2 erhöhen, wenn keine Pixy-Wert;
    }
}